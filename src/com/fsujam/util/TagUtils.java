package com.fsujam.util;

/**
 * @author Rowell Belen
 */
public class TagUtils
{
  private static String APP_NAME = "FSUJam";

  public static String getTag(Object o){
    if(o != null){
      return getTag(o.getClass());
    }

    return APP_NAME;
  }

  public static String getTag(Class<?> c){
    if(c != null){
      return APP_NAME + "." + c.getName();
    }

    return APP_NAME;
  }
}
