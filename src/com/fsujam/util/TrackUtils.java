package com.fsujam.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import com.fsujam.R;
import com.fsujam.model.Track;

/**
 * @author Rowell Belen
 */
public class TrackUtils {

 public static void setAlbumArt(Context context, ImageView iv, Track track){
  try{
    if (track.getAlbumArt() != null){
      Bitmap bmp = BitmapFactory.decodeFile(track.getAlbumArt());
      iv.setImageBitmap(bmp);
    } else{
      iv.setImageDrawable(context.getResources().getDrawable(R.drawable.blank_album_art));
    }
  }
  catch (Exception e){
    e.printStackTrace();
  }
 }
}
