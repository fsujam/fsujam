package com.fsujam.util;

import android.util.Log;

/**
 * @author Rowell Belen
 */
public class TrackParsingUtils {
  public static int parseTrackNumber(String trackNumber) {
    //strips all chars beginning at first non digit
    String strippedTrackNumber = trackNumber.replaceAll("\\D.*", "");

    if (strippedTrackNumber.length() > 0) {
      return Integer.parseInt(strippedTrackNumber);
    }
    return 0;
  }

  public static double parseDuration(String duration) {
    try {
      return Double.parseDouble(duration);
    }
    catch (NumberFormatException e) {
      Log.v(TagUtils.getTag(TrackParsingUtils.class), "Unable to parse duration '" + duration + "': " + e.getMessage());
    }
    return 0;
  }
}
