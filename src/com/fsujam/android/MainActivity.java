package com.fsujam.android;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import com.fsujam.model.Artist;
import com.fsujam.model.Playlist;
import com.fsujam.model.Track;
import com.fsujam.persistence.*;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class MainActivity extends Activity {
  /**
   * Called when the activity is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //setContentView(R.layout.main);

    // instantiate track manager
    LibraryManager libraryManager = new LibraryDBManager(this);

    // scan music directory and build database
    File mediaRoot = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
    FsReader.scanDir(libraryManager, mediaRoot);

    // get all tracks
    List<Track> tracks = libraryManager.getTracks();
    for(Track track : tracks){
      Log.d(this.getClass().getName(), track.getNumber() + " - " + track.getTrackId() + " - " + track.getTitle());
    }

    // get specific track
    long trackId = 1;
    Track track = libraryManager.getTrack(trackId);

    Log.d(this.getClass().getName(), "libraryManager.getTrack - " + track.getArtist().getName());

    // get artists
    Set<Artist> artists = libraryManager.getArtists();
    for(Artist artist : artists){
      Log.d(this.getClass().getName(), "libraryManager.getArtists - " + artist.getName());

      // get tracks by artist
      Set<Track> trackSet = libraryManager.getTracksByArtist(artist.getName());
      Iterator<Track> it = trackSet.iterator();
      while(it.hasNext()){
        Log.d(this.getClass().getName(), "libraryManager.getTracksByArtist - " + it.next().getTitle());
      }
    }

    // instantiate playlist manager
    PlaylistManager playlistManager = new PlaylistDBManager(this, libraryManager);

    // create new playlist
    Playlist playlist = playlistManager.createNewPlaylist("Test Playlist");

    // get all playlists
    List<Playlist> playlists = playlistManager.getPlaylists();
    for(Playlist p : playlists){
      Log.d(this.getClass().getName(), "playlistManager.getPlaylists - " + p.getPlaylistId() +
         " " + p.getPlaylistName());
    }

    // add track to playlist
    long playlistId = playlists.get(0).getPlaylistId();
    playlistManager.addToPlaylist(playlistId, track.getTrackId());

    // get all tracks in playlist
    tracks = playlistManager.getTracksByPlayList(playlistId);
    for(Track t : tracks){
      Log.d(this.getClass().getName(), "playlistManager.getTracksByPlayList - " + t.getTitle());
    }

    // remove track from playlist
    playlistManager.removeFromPlaylist(playlistId, track.getTrackId());

    // rename playlist
    playlistManager.renamePlaylist(playlistId, "New Playlist Name");

    // delete playlist
    playlists = playlistManager.getPlaylists(); // retrieve playlist again
    Log.d(this.getClass().getName(), "before delete playlist - " + playlists.size());
    playlistManager.deletePlaylist(playlists.get(0).getPlaylistId()); // delete
    playlists = playlistManager.getPlaylists(); // retrieve playlist again
    Log.d(this.getClass().getName(), "after delete playlist - " + playlists.size());

    if(true){
      return;
    }
  }
}
