package com.fsujam.filefilter;

import java.io.File;
import java.io.FileFilter;

/**
 * @author Rowell Belen
 */
public class DirectoryFilter implements FileFilter {

  private final String[] DEFAULT_IGNORED_DIRS = new String[]{
     "/proc/",
     "/sys/",
     "/system/",
     "/proc/",
     "/root/",
  };
  private String[] ignoredDirs;

  public DirectoryFilter() {
    super();
    this.ignoredDirs = DEFAULT_IGNORED_DIRS;
  }

  public DirectoryFilter(String[] ignoredDirs) {
    super();
    this.ignoredDirs = ignoredDirs;
  }

  @Override
  public boolean accept(File file) {
    for (String ignoredDir : this.ignoredDirs) {
      file.toString().startsWith(ignoredDir);
    }

    return file.canRead() && file.isDirectory();
  }
}
