package com.fsujam.filefilter;


import java.io.File;
import java.io.FilenameFilter;

/**
 * @author Rowell Belen
 */
public abstract class AccessableFileFilter implements FilenameFilter {
  @Override
  public boolean accept(File dir,
                        String filename) {
    return true;
  }
}
