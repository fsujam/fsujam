package com.fsujam.filefilter;

import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.List;

/**
 * @author Rowell Belen
 */
public interface FileFilters {
  final static FilenameFilter PLAYABLE_FILES_FILTER = new NameFilter(".*.mp3", ".*.ogg");

  final static List<? extends FilenameFilter> folderArtFilters = Arrays.asList(
     new NameFilter("folder.jpg", "folder.png"),
     new NameFilter(".*front.*.jpg", ".*front.*.jpeg", ".*front.*.gif", ".*front.*.png"),
     new NameFilter(".*cover.*.jpg", ".*cover.*.jpeg", ".*cover.*.gif", ".*cover.*.png"),
     new NameFilter(".*.jpg", ".*.jpeg", ".*.gif", ".*.png")
  );
}