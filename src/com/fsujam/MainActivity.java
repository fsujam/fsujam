package com.fsujam;

/* Group: Android MOB
 * @author G. Maturana
 * 
 */

import java.io.File;
import java.util.ArrayList;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import android.media.MediaPlayer;

import android.media.AudioManager;
import android.os.Bundle;
import android.os.Environment;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Dialog;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ViewFlipper;
import android.view.LayoutInflater;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.SeekBar;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TableRow;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.EditText;
import java.io.FilenameFilter;
import android.widget.AdapterView;

import android.app.ListActivity;



import com.fsujam.model.Artist;
import com.fsujam.model.Playlist;
import com.fsujam.model.Track;
import com.fsujam.model.Album;
import com.fsujam.persistence.*;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class MainActivity extends Activity implements View.OnClickListener,
AdapterView.OnItemClickListener{
	
	ViewFlipper songImage;
	Button songBtn, plistBtn, albumBtn, artistBtn, nowPlayingBtn;
	TableRow row1;
	SeekBar volumeControl;
	SeekBar trackControl;
	AudioManager control;
	ListView sList, artList, Plist;
	Dialog dialog;
	Button addPL;

	private PlaylistManager playlistManager;
	private LibraryManager libraryManager;
//	Playlist_Dialogs myDialogs;
	
    // instantiate track manager
    int number = 1;
    Track track;
    MediaPlayer Song;
	
    @Override
    public void onCreate(Bundle icicle) {
    	super.onCreate(icicle);
    	//super.onCreate(savedInstanceState);
        setContentView(R.layout.play_list);
        
        libraryManager = new LibraryDBManager(this);
        playlistManager = new PlaylistDBManager(this, libraryManager);
        
        // scan music directory and build database
        File mediaRoot = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
        FsReader.scanDir(libraryManager, mediaRoot);
        
        
        ArrayList<String> tracklist = new ArrayList<String>();
        // get all tracks
        List<Track> tracks = libraryManager.getTracks();
        for(Track tk : tracks){
         tracklist.add(tk.getNumber() +"-" + tk.getTitle());
        }
        // Adding information to the list
    	sList = (ListView)findViewById(R.id.mlist);
    	sList.addHeaderView(Header("ALL SONGS"));
    	
    	ArrayAdapter<String> listAdapter = 
    		new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,tracklist);
    	sList.setAdapter(listAdapter);

        // get specific track
         sList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	 
             public void onItemClick(AdapterView<?> parent, View view,
                     int position, long id) {
                 // getting list item index
                 int sIndex = (int)parent.getItemIdAtPosition(position);
 
                 // Starting new intent
                 Intent in = new Intent(getApplicationContext(),
                         MusicPlayerMain.class);
                 // Sending songIndex to PlayerActivity
                 in.putExtra("sIndex", sIndex);
                 setResult(100, in);
                 startActivity(in);
                 // Closing PlayListView
                 finish();
             }
         });
         
         // get artists
         ArrayList<String> artistList = new ArrayList<String>();
         Set<Artist> artists = libraryManager.getArtists();
         for(Artist artist : artists){
        
             // get tracks by artist
             Set<Track> trackSet = libraryManager.getTracksByArtist(artist.getName());
             Iterator<Track> it = trackSet.iterator();
             while(it.hasNext()){
               artistList.add(artist.getName() + ": " + it.next().getTitle());
             }
         }
         
         ArrayAdapter<String> Alist = 
        	 new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,artistList);
         artList = (ListView)findViewById(R.id.alist);
         artList.addHeaderView(Header("ARTIST PlAYLIST"));
         artList.setAdapter(Alist);
         
        // ArrayList<String> albumList = new ArrayList<String>();
        // Set<Album> album = libraryManager.get
         
         
        songBtn = (Button)findViewById(R.id.songsBtn);
        songBtn.setOnClickListener(this);
        artistBtn = (Button)findViewById(R.id.artistBtn);
        artistBtn.setOnClickListener(this);
        nowPlayingBtn = (Button)findViewById(R.id.nowPlaying);
        nowPlayingBtn.setOnClickListener(this);
        
      }
    @Override
    public void onClick(View view)
    {
    	//Playlist_Dialogs myDialog = new Playlist_Dialogs();
    	// Switch items on the main screen
    	switch (view.getId()){
    	case R.id.artistBtn:
    	{
    		artList.setVisibility(view.VISIBLE);
    		sList.setVisibility(view.GONE);
    		//Plist.setVisibility(view.GONE);
    	}
    		break;    	
    	case R.id.nowPlaying:
    	{
            Intent in = new Intent(getApplicationContext(), MusicPlayerMain.class);
            startActivity(in);
    	}
    		break;
    	case R.id.songsBtn:
    	{
    		artList.setVisibility(view.GONE);
    		sList.setVisibility(view.VISIBLE);
    		
    	}
    		break;
    	}
    }
	// Add Playlist
	public void AddPlaylistDialog()
	{
		LibraryManager trackManager = new LibraryDBManager(this);
		final PlaylistManager playlistManager = new PlaylistDBManager(this, trackManager);
		LayoutInflater inflater = getLayoutInflater();
    	View layoutDialog = inflater.inflate(R.layout.add_play_list, null);
    	final EditText playlistName = (EditText)layoutDialog.findViewById(R.id.palyListName);
    	

    	// creates the dialog it must be set to final otherwise buttons won't work
    	final Dialog dialogAddPlaylist = new Dialog(this);
    	Button cancelBtn = (Button)layoutDialog.findViewById(R.id.cancelBtn);
    	Button saveBtn = (Button)layoutDialog.findViewById(R.id.saveBtn);
    	
    	
    	
    	// set the content view of the dialog
    	dialogAddPlaylist.setContentView(layoutDialog);
    	//
    	
    	// displays the dialog
    	dialogAddPlaylist.getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    	dialogAddPlaylist.show();
    	
    	// Button Functions
    	// Cancel Button
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	//Closes dialog message without making changes
            	dialogAddPlaylist.dismiss();
            }
        });
    	
        //Save Button
        saveBtn.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	// turns the text store in playlistName and turns it into a string
            	
            	String plist = " ";
            	plist = playlistName.getText().toString();
            	
            	playlistManager.createNewPlaylist(plist); // Cereate new playlist
            	dialogAddPlaylist.dismiss();
            }
        });
    	
	}
	public void ClearPlaylistDialog()
	{
		LayoutInflater inflater = getLayoutInflater();
		View layoutDialog = inflater.inflate(R.layout.clear_playlist, null);

	
		// creates the dialog it must be set to final otherwise buttons won't work
		final Dialog dialogClearPlaylist = new Dialog(this);
		Button cancelClearBtn = (Button)layoutDialog.findViewById(R.id.cancelClearBtn);
		Button clearBtn = (Button)layoutDialog.findViewById(R.id.clearBtn);
		
		// set the content view of the dialog
		dialogClearPlaylist.setContentView(layoutDialog);
		
		// sets the width and height of the playlist
		dialogClearPlaylist.getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		// displays playlist
		dialogClearPlaylist.show();
		
    	// Button Functions
    	// Cancel Button
        cancelClearBtn.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	//Closes dialog message without making changes
            	dialogClearPlaylist.dismiss();
            }
        });
    	
        //Save Button
        clearBtn.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	//clears all the songs stored in the playlist, but does not delete the playlist
            	
            	dialogClearPlaylist.dismiss();
            }
        });
	
	}
	public void DeletePlaylistDialog()
	{
		LayoutInflater inflater = getLayoutInflater();
		View layoutDialog = inflater.inflate(R.layout.delete_playlist, null);

	
		// creates the dialog it must be set to final otherwise buttons won't work
		final Dialog dialogDeletePlaylist = new Dialog(this);
		Button cancelDeleteBtn = (Button)layoutDialog.findViewById(R.id.cancelDeleteBtn);
		Button deleteBtn = (Button)layoutDialog.findViewById(R.id.deleteBtn);
		
		// set the content view of the dialog
		dialogDeletePlaylist.setContentView(layoutDialog);
		
		// sets the width and height of the playlist
		dialogDeletePlaylist.getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		// displays playlist
		dialogDeletePlaylist.show();
		
    	// Button Functions
    	// Cancel Button
        cancelDeleteBtn.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	//Closes dialog message without making changes
            	dialogDeletePlaylist.dismiss();
            }
        });
    	
        //Save Button
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	//deletes playlist from the playlist menu
            	
            	dialogDeletePlaylist.dismiss();
            }
        });
	
	}
	// Add Playlist Button
	// on listview when the plalist button is pressed
	private View AddPlaylist()
	{
		Button btn = new Button(this);
		btn.setText("Add Playlist");
		btn.setBackgroundResource(R.drawable.button_corners);
		btn.setWidth(70);
		btn.setHeight(20);
		
		btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Displays the Add playlist dialog
				AddPlaylistDialog();
			}
		});
		
		return (btn);
		
	}
	
	//General Headers
	private View Header(String arg)
	{
		TextView text = new TextView(this);
		text.setText(arg);
		text.setBackgroundColor(Color.YELLOW);
		text.setTextColor(Color.RED);
		return (text);
	}
	
	// Play list header
	// --------------------------------
	// edit | delete | clear
	private View EditPlaylistHeader()
	{
		LayoutInflater inflater = getLayoutInflater();
		View menu = inflater.inflate(R.layout.playlist_menu, null);
		Button editBtn = (Button)menu.findViewById(R.id.editPlist);
		Button deleteBtn = (Button)menu.findViewById(R.id.deletePlist);
		Button clear = (Button)menu.findViewById(R.id.clearPlist);
		
		editBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AddPlaylistDialog();
			}
		});
		deleteBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DeletePlaylistDialog();
			}
		});
		clear.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClearPlaylistDialog();
			}
		});
		
		return (menu);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}

}
