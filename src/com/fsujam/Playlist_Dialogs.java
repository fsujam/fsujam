package com.fsujam;
/*
 * @author Gustavo Maturana
 * @group Android MOB
 */
import com.fsujam.model.Playlist;
import com.fsujam.persistence.LibraryDBManager;
import com.fsujam.persistence.LibraryManager;
import com.fsujam.persistence.PlaylistDBManager;
import com.fsujam.persistence.PlaylistManager;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.app.ActionBar.LayoutParams;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.content.Context;
import android.widget.ListAdapter;


public abstract class Playlist_Dialogs extends Activity{

	// Add Playlist
	
	public void AddPlaylistDialog()
	{
		LibraryManager trackManager = new LibraryDBManager(this);
		final PlaylistManager playlistManager = new PlaylistDBManager(this, trackManager);
		LayoutInflater inflater = getLayoutInflater();
    	View layoutDialog = inflater.inflate(R.layout.add_play_list, null);
    	

    	// creates the dialog it must be set to final otherwise buttons won't work
    	final Dialog dialogAddPlaylist = new Dialog(this);
    	Button cancelBtn = (Button)layoutDialog.findViewById(R.id.cancelBtn);
    	Button saveBtn = (Button)layoutDialog.findViewById(R.id.saveBtn);
    	final EditText playlistName = (EditText)layoutDialog.findViewById(R.id.playListTxt);
    	
    	// set the content view of the dialog
    	dialogAddPlaylist.setContentView(layoutDialog);
    	//
    	dialogAddPlaylist.setTitle("Add Playlist");
    	// displays the dialog
    	dialogAddPlaylist.getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    	dialogAddPlaylist.show();
    	
    	// Button Functions
    	// Cancel Button
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	//Closes dialog message without making changes
            	dialogAddPlaylist.dismiss();
            }
        });
    	
        //Save Button
        saveBtn.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	// turns the text store in playlistName and turns it into a string
            	Context context = null;
            	LibraryManager libraryManager = new LibraryDBManager(context);
            	PlaylistManager playlistManager = new PlaylistDBManager(context,libraryManager);
            	String plist = playlistName.toString();
            	Playlist newPlaylist;
            	newPlaylist = playlistManager.createNewPlaylist(plist);
            	dialogAddPlaylist.dismiss();
            }
        });
    	
	}
	public void ClearPlaylistDialog()
	{
		LayoutInflater inflater = getLayoutInflater();
		View layoutDialog = inflater.inflate(R.layout.clear_playlist, null);

	
		// creates the dialog it must be set to final otherwise buttons won't work
		final Dialog dialogClearPlaylist = new Dialog(this,R.style.My_Dialog);
		Button cancelClearBtn = (Button)layoutDialog.findViewById(R.id.cancelClearBtn);
		Button clearBtn = (Button)layoutDialog.findViewById(R.id.clearBtn);
		
		// set the content view of the dialog
		dialogClearPlaylist.setContentView(layoutDialog);
		
		// sets the width and height of the playlist
	//	dialogClearPlaylist.getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		// displays playlist
		dialogClearPlaylist.show();
		
    	// Button Functions
    	// Cancel Button
        cancelClearBtn.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	//Closes dialog message without making changes
            	dialogClearPlaylist.dismiss();
            }
        });
    	
        //Save Button
        clearBtn.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	//clears all the songs stored in the playlist, but does not delete the playlist
            	
            	dialogClearPlaylist.dismiss();
            }
        });
	
	}
	public void DeletePlaylistDialog()
	{
		LayoutInflater inflater = getLayoutInflater();
		View layoutDialog = inflater.inflate(R.layout.delete_playlist, null);

	
		// creates the dialog it must be set to final otherwise buttons won't work
		final Dialog dialogDeletePlaylist = new Dialog(this,R.style.My_Dialog);
		Button cancelDeleteBtn = (Button)layoutDialog.findViewById(R.id.cancelDeleteBtn);
		Button deleteBtn = (Button)layoutDialog.findViewById(R.id.deleteBtn);
		
		// set the content view of the dialog
		dialogDeletePlaylist.setContentView(layoutDialog);
		
		// sets the width and height of the playlist
		dialogDeletePlaylist.getWindow().setLayout(500,320);
		// displays playlist
		dialogDeletePlaylist.show();
		
    	// Button Functions
    	// Cancel Button
        cancelDeleteBtn.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	//Closes dialog message without making changes
            	dialogDeletePlaylist.dismiss();
            }
        });
    	
        //Save Button
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	//deletes playlist from the playlist menu
            	
            	dialogDeletePlaylist.dismiss();
            }
        });
	
	}

}
