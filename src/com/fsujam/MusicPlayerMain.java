package com.fsujam;

/*
 * @group Android MOB
 * @autor Gustavo Maturana
 */

import java.io.IOException;

import com.fsujam.model.Playlist;

import android.app.Activity;
import android.app.Dialog;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.AudioManager;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Button;
import android.widget.ViewFlipper;
import android.widget.TextView;
import android.widget.ImageButton;


import com.fsujam.model.Artist;
import com.fsujam.model.Playlist;
import com.fsujam.model.Track;
import com.fsujam.model.Album;
import com.fsujam.persistence.LibraryDBManager;
import com.fsujam.persistence.LibraryManager;
import com.fsujam.persistence.PlaylistDBManager;
import com.fsujam.persistence.PlaylistManager;
import com.fsujam.util.TrackUtils;

import java.util.List;



public class MusicPlayerMain extends Activity implements OnCompletionListener, 
SeekBar.OnSeekBarChangeListener {
	
	// music player buttons
	private Button playBtn, forwardBtn, backwardBtn;
	
	private Button backPlaylist;
	private SeekBar volumeControl;
	private ImageView songImage;
	private TextView songTitle;
	private MediaPlayer mplayer = new MediaPlayer();
	private AudioManager control;
	
	private Track cover;
	
	private PlaylistManager playlistManager;
	private LibraryManager libraryManager;
	private int index;	//index of song
	private int result;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.music_main);

    // get playBtn component
    playBtn = (Button)findViewById(R.id.playBtn);
		
		// Getting the intent from MainActivity
		Bundle extras = getIntent().getExtras(); 
		if (extras != null) {
			 result= extras.getInt("sIndex");
		}
		if(extras == null)
		{ 
			result = 13;
		    // and get whatever type user account id is
		}
        libraryManager = new LibraryDBManager(this);
        playlistManager = new PlaylistDBManager(this, libraryManager);
        
        
		songTitle = (TextView)findViewById(R.id.songTitle); // Artist and Song Title
		// Image Icon
		songTitle.setText(libraryManager.getTrack((long)result).getSrc());
		//songTitle.setText(getString(index));
	    songImage = (ImageView)findViewById(R.id.viewFlipper1);
	    
	    playNow((long)result);
	    //Log.d(this.getClass().getName(), " This is the Id: "+ tk.getTrackId() + "Art:" +tk.getAlbumArt());
        songImage.setOnClickListener(new View.OnClickListener() {
        
            @Override
            public void onClick(View arg0) {
            	JamsMenu();
            }
        });

        backwardBtn = (Button)findViewById(R.id.backwardBtn);
        backwardBtn.setText("<<");
        backwardBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
	               if(result > 0){
	                    playNow(result - 1);
	                    result--;
	                }else{
	                    // play last song
	                    playNow(libraryManager.getTracks().size() - 1);
	                    result = libraryManager.getTracks().size() -1;
	                }
			}
		});
        
        // play button
        playBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//checks if music is already playing
				if(mplayer.isPlaying())
				{
					if(mplayer!=null)
						mplayer.pause();
						//change the label on play button
						playBtn.setText(">");
				}
				else
				{
					if(mplayer!=null)
					{
						mplayer.start();
						// change the label on play button
						playBtn.setText("||");
					}
				}
			}
		});
        
        // forward button
        forwardBtn = (Button)findViewById(R.id.forwardBtn);
        forwardBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
                // check if next song is there or not
                if(result < (libraryManager.getTracks().size() - 1)){
                    playNow((long)result + 1);
                    result++;
                }else{
                    // play first song
                    playNow(0);
                    result = 0;
                }
			}
		});
        
        // Back to play list button
        backPlaylist = (Button)findViewById(R.id.backpListBtn);
        backPlaylist.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
	             Intent i = new Intent(getApplicationContext(), MainActivity.class);
	                startActivityForResult(i, 100);
			}
		});
        
        //Volume Control
        VolumeControls();
			
		}
	

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCompletion(MediaPlayer arg0) {
		// TODO Auto-generated method stub
		
	}
	//Volume Control
    private void VolumeControls()
    {
        try
        {
            volumeControl = (SeekBar)findViewById(R.id.volumeControl);
            control = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            volumeControl.setMax(control.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            
            volumeControl.setProgress(control.getStreamVolume(AudioManager.STREAM_MUSIC));   


            volumeControl.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
            {
                @Override
                public void onStopTrackingTouch(SeekBar arg0) 
                {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0) 
                {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) 
                {
                	
                    control.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);
                }
            });
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
    }
    
    //Music Control
    public void JamsMenu()
    {
    	LayoutInflater inflater = getLayoutInflater();
    	View layout = inflater.inflate(R.layout.jams_menu, null);
        Dialog JamsMenuDialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        JamsMenuDialog.setContentView(layout);
    	ImageButton shuffle = (ImageButton)layout.findViewById(R.id.shuffleBtn);
    	ImageButton repeat = (ImageButton)layout.findViewById(R.id.repeatBtn);
    	TextView currentTime = (TextView)layout.findViewById(R.id.currentTime);
    	TextView remainingTime = (TextView)layout.findViewById(R.id.remaningTime);
    	SeekBar musicProgress = (SeekBar)layout.findViewById(R.id.musicProgress);
    	
    	// displays the dialog
    	JamsMenuDialog.getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);//size of the display
    	// dialog is dissmiss after is click outside of dialog
    	JamsMenuDialog.setCanceledOnTouchOutside(true);
    	JamsMenuDialog.show();
    	
    	//Button functions
    	
        //shuffle Button
        shuffle.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	// turns the text store in playlistName and turns it into a string
            	//dismisses the dialog box
            	
            }
        });
        
        //Repeat Button
        repeat.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
            	// turns the text store in playlistName and turns it into a string
            	//dismisses the dialog box
            	
            }
        });
     
    }
   
	public void  playNow(long TrackIndex){
        // Play song
        try {
        	//Uri music = ;
        	mplayer.reset();
        	mplayer.setDataSource(libraryManager.getTrack(TrackIndex).getSrc());
            mplayer.prepare();
            mplayer.start();
            // Displaying Song title
            String songInfo = libraryManager.getTrack(TrackIndex).getArtist().getName() + "\n" +
            					libraryManager.getTrack(TrackIndex).getTitle();
            songTitle.setText(songInfo);
            TrackUtils.setAlbumArt(this, songImage, libraryManager.getTrack(TrackIndex));
            
            // Change the  pbutton to pause '||'
            playBtn.setText("||");
            // set Progress bar values
       //     songProgressBar.setProgress(0);
        //    songProgressBar.setMax(100);
 
            // Updating progress bar
          //  updateProgressBar();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
	

}
