package com.fsujam.model;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Rowell Belen
 */
public class Track {

  private long trackId;
  private String title;
  private int number;
  private Artist artist;
  private Album album;
  private double length;
  private String src;
  private String rootSrc;
  private String albumArt;

  public Track(String title,
               int number,
               Artist artist,
               Album album,
               double length,
               String src,
               String rootSrc,
               String albumArt) {
    this.title = title;
    this.number = number;
    this.artist = artist;
    this.album = album;
    this.length = length;
    this.src = src;
    this.rootSrc = rootSrc;
    this.albumArt = albumArt;
  }

  public String getTitle() {
    return title;
  }

  public int getNumber() {
    return number;
  }

  public Artist getArtist() {
    return artist;
  }

  public Album getAlbum() {
    return album;
  }

  public double getLength() {
    return length;
  }

  public String getSrc() {
    return src;
  }

  public String getRootSrc() {
    return rootSrc;
  }

  public String getAlbumArt() {
    return albumArt;
  }

  public long getTrackId() {
    return trackId;
  }

  public void setTrackId(int id) {
    this.trackId = id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Track track = (Track) o;

    if (!src.equals(track.src)) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return src.hashCode();
  }
}