package com.fsujam.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rowell Belen
 */
public class Playlist {

  private long playlistId;
  private String playlistName;
  private List<Track> tracks;

  public Playlist(){
    this.tracks = new ArrayList<Track>();
  }

  public long getPlaylistId() {
    return playlistId;
  }

  public void setPlaylistId(long playlistId) {
    this.playlistId = playlistId;
  }

  public String getPlaylistName() {
    return playlistName;
  }

  public void setPlaylistName(String playlistName) {
    this.playlistName = playlistName;
  }

  public List<Track> getTracks() {
    return tracks;
  }

  public void setTracks(List<Track> tracks) {
    this.tracks = tracks;
  }
}
