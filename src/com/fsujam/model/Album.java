package com.fsujam.model;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Rowell Belen
 */
public class Album {
  private final String name;

  public Album(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Album album = (Album) o;

    if (!name.equals(album.name)) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }
}
