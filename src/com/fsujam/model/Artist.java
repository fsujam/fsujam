package com.fsujam.model;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Rowell Belen
 */
public class Artist {

  private final String name;

  public Artist(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Artist artist = (Artist) o;

    if (!name.equals(artist.name)) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }
}
