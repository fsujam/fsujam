package com.fsujam.persistence;

import android.media.MediaMetadataRetriever;
import android.util.Log;
import com.fsujam.filefilter.FileFilters;
import com.fsujam.model.Album;
import com.fsujam.model.Artist;
import com.fsujam.model.Track;
import com.fsujam.filefilter.DirectoryFilter;
import com.fsujam.util.Shorty;
import com.fsujam.util.TagUtils;
import com.fsujam.util.TrackParsingUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;

/**
 * @author Rowell Belen
 */
public class FsReader {

  public static final int MAX_DIR_SCAN_DEPTH = 50;
  final static FileFilter isDIR = new DirectoryFilter();


  public static void scanDir(LibraryManager libraryManager,
                             File rootNode) {
    MediaMetadataRetriever metaDataReader = new MediaMetadataRetriever();
    scanDir(libraryManager, metaDataReader, rootNode, 0, null);
    metaDataReader.release();
  }

  /**
   * @param rootNode
   * @param depth    number of parent allready visited
   */
  private static void scanDir(LibraryManager libraryManager,
                              MediaMetadataRetriever metaDataReader,
                              File rootNode,
                              int depth,
                              String parentAlbumArt) {
    // http://www.exampledepot.com/egs/java.io/GetFiles.html

    try {
      String albumArt = getAlbumArt(rootNode);

      if (albumArt != null) {
        albumArt = rootNode + "/" + albumArt;
      }
      else {
        albumArt = parentAlbumArt;
      }

      for (String mp3 : rootNode.list(FileFilters.PLAYABLE_FILES_FILTER)) {
        Log.v(TagUtils.getTag(FsReader.class), "register " + rootNode + "/" + mp3);
        metaDataReader.setDataSource(rootNode + "/" + mp3);

        String title = extractMetadata(metaDataReader, MediaMetadataRetriever.METADATA_KEY_TITLE);
        int number = TrackParsingUtils.parseTrackNumber(extractMetadata(metaDataReader, MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER));
        double length = TrackParsingUtils.parseDuration(extractMetadata(metaDataReader, MediaMetadataRetriever.METADATA_KEY_DURATION));
        String artist = extractMetadata(metaDataReader, MediaMetadataRetriever.METADATA_KEY_ARTIST);
        String album = extractMetadata(metaDataReader, MediaMetadataRetriever.METADATA_KEY_ALBUM);

        if (Shorty.isVoid(title)) {
          title = "Unknown";
        }

        if (Shorty.isVoid(artist)) {
          artist = "Unknown";
        }

        if (Shorty.isVoid(album)) {
          album = "Unknown";
        }

        Track t = new Track(
           title,
           number,
           new Artist(artist),
           new Album(album),
           length,
           rootNode + "/" + mp3,
           rootNode + "/",
           albumArt
        );
        Log.d(TagUtils.getTag(FsReader.class), title + " - " + albumArt);

        libraryManager.add(t);
      }

      for (File dir : rootNode.listFiles(isDIR)) {
        if (depth < MAX_DIR_SCAN_DEPTH) // avoid Stack overflow - symbolic link points to a parent dir
        {
          scanDir(libraryManager, metaDataReader, dir, depth + 1, albumArt);
        }
      }
    }
    catch (NullPointerException e) {
      // Probably No SD-Card
      Log.v(TagUtils.getTag(FsReader.class), e.getMessage());
    }
  }

  /**
   * calls {@link android.media.MediaMetadataRetriever#extractMetadata(int)} and removes
   * chunk after 0 terminated string
   *
   * @param keyCode see {@link android.media.MediaMetadataRetriever#extractMetadata(int)}
   * @return
   */
  private static String extractMetadata(MediaMetadataRetriever metaDataReader,
                                int keyCode) {
    String metaData = Shorty.avoidNull(metaDataReader.extractMetadata(keyCode));

    //replace all chars exept letters and digits, space and dash
    //return metaData.replaceAll("^\\w\\s-,:;?$[]\"]","");

    int indexOfZeroTermination = metaData.indexOf(0);
    return indexOfZeroTermination < 0 ? metaData : metaData.substring(0, indexOfZeroTermination);
  }

  private static String getAlbumArt(File dir) {
    for (FilenameFilter filenameFilter : FileFilters.folderArtFilters) {
      String[] paths = dir.list(filenameFilter);
      if (paths.length > 0) {
        return paths[0];
      }
    }
    return null;
  }
}
