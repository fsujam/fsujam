package com.fsujam.persistence;

import com.fsujam.model.Artist;
import com.fsujam.model.Track;
import com.fsujam.persistence.utils.DbObserverAware;

import java.util.List;
import java.util.Set;

/**
 * @author Rowell Belen
 */
public interface LibraryManager extends DbObserverAware {

  public void clear();

  public boolean isEmpty();

  public void add(final Track track);

  public List<Track> getTracks();

  public Track getTrack(long trackId);

  public Set<Artist> getArtists();

  public Set<Track> getTracksByArtist(String artistName);
}
