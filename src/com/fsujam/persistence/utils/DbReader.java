package com.fsujam.persistence.utils;

import android.database.sqlite.SQLiteDatabase;

/**
 * @author Rowell Belen
 */
public interface DbReader<R> {
  R read(SQLiteDatabase db);
}
