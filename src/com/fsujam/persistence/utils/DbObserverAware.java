package com.fsujam.persistence.utils;

/**
 * @author Rowell Belen
 */
public interface DbObserverAware {
  public void addObserver(DbObserver observer);

  public void removeObserver(DbObserver observer);
}
