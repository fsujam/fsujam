package com.fsujam.persistence.utils;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Rowell Belen
 */
public class DbUtils {

  public static <R> R read(SQLiteOpenHelper helper, DbReader<R> dbOperation) {
    SQLiteDatabase db = helper.getReadableDatabase();

    try {
      return dbOperation.read(db);
    }
    finally {
      db.close();
    }
  }

  public static void write(SQLiteOpenHelper helper, DbWriter writer) {
    SQLiteDatabase db = helper.getReadableDatabase();

    try {
      writer.write(db);
    }
    finally {
      db.close();
    }
  }
}
