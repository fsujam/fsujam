package com.fsujam.persistence.utils;

import com.fsujam.model.Track;

/**
 * @author Rowell Belen
 */
public interface DbObserver {
  void trackAdded(Track track);

  void cleaned();
}
