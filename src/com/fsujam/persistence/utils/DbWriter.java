package com.fsujam.persistence.utils;

import android.database.sqlite.SQLiteDatabase;

/**
 * @author Rowell Belen
 */
public interface DbWriter {
  public void write(SQLiteDatabase db);
}
