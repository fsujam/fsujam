package com.fsujam.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.fsujam.model.Playlist;
import com.fsujam.model.Track;
import com.fsujam.persistence.utils.DbObserver;
import com.fsujam.persistence.utils.DbReader;
import com.fsujam.persistence.utils.DbUtils;
import com.fsujam.persistence.utils.DbWriter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rowell Belen
 */
public class PlaylistDBManager extends BaseDBManager implements PlaylistManager {

  private static final int DATABASE_VERSION = 2;
  private static final String DATABASE_NAME = "FSUJam";
  private static final String TABLE_PLAYLIST = "Playlist";
  private static final String TABLE_PLAYLIST_TRACKS = "PlaylistTracks";

  private static final String KEY_PLAYLIST_ID = "playlistId";
  private static final String KEY_PLAYLIST_NAME = "playlistName";
  private static final String KEY_TRACK_ID = "trackId";

  private LibraryManager libraryManager;

  public PlaylistDBManager(Context context, LibraryManager libraryManager) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
    this.libraryManager = libraryManager;
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    String CREATE_PLAYLIST_TABLE = "CREATE TABLE " + TABLE_PLAYLIST + " ("
       + KEY_PLAYLIST_ID + " INTEGER PRIMARY KEY, "
       + KEY_PLAYLIST_NAME + " TEXT);";
    db.execSQL(CREATE_PLAYLIST_TABLE);

    String CREATE_PLAYLIST_TRACKS_TABLE = "CREATE TABLE " + TABLE_PLAYLIST_TRACKS + " ("
       + KEY_PLAYLIST_ID + " INTEGER, "
       + KEY_TRACK_ID + " INTEGER, " +
       "PRIMARY KEY (" + KEY_PLAYLIST_ID + " , " + KEY_TRACK_ID + "));";
    db.execSQL(CREATE_PLAYLIST_TRACKS_TABLE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db,
                        int oldVersion,
                        int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYLIST);
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYLIST_TRACKS);
    onCreate(db);
  }

  @Override
  protected String[] getTableNamesToClear() {
    return new String[]{TABLE_PLAYLIST, TABLE_PLAYLIST_TRACKS};
  }

  @Override
  protected String getTableNameToCheckForEmpty() {
    return TABLE_PLAYLIST;
  }

  @Override
  public List<Playlist> getPlaylists() {
    return DbUtils.read(this, new DbReader<List<Playlist>>() {
      @Override
      public List<Playlist> read(SQLiteDatabase db) {
        List<Playlist> pList = new ArrayList<Playlist>();
        String query = "SELECT * FROM " + TABLE_PLAYLIST;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
          do {

            Playlist p = new Playlist();
            p.setPlaylistId(Long.parseLong(cursor.getString(0)));
            p.setPlaylistName(cursor.getString(1));
            p.setTracks(getTracksByPlayList(p.getPlaylistId()));

            pList.add(p);

          } while (cursor.moveToNext());
        }

        return pList;
      }
    });
  }

  @Override
  public Playlist createNewPlaylist(final String playlistName) {

    final Playlist p = new Playlist();
    DbUtils.write(this, new DbWriter() {
      @Override
      public void write(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(KEY_PLAYLIST_NAME, playlistName);
        long rowId = db.insert(TABLE_PLAYLIST, null, values);
        p.setPlaylistId(rowId);
        p.setPlaylistName(playlistName);
      }
    });

    return p;
  }

  @Override
  public void deletePlaylist(final long playlistId) {
    DbUtils.write(this, new DbWriter() {
      @Override
      public void write(SQLiteDatabase db) {
        String[] args = {"" + playlistId};

        db.delete(TABLE_PLAYLIST_TRACKS, KEY_PLAYLIST_ID + "=?", args);
        db.delete(TABLE_PLAYLIST, KEY_PLAYLIST_ID + "=?", args);
      }
    });
  }

  @Override
  public void renamePlaylist(final long playlistId, final String newPlaylistName) {
    DbUtils.write(this, new DbWriter() {
      @Override
      public void write(SQLiteDatabase db) {
        String[] args = {"" + playlistId};
        ContentValues values = new ContentValues();
        values.put(KEY_PLAYLIST_NAME, newPlaylistName);
        db.update(TABLE_PLAYLIST, values, KEY_PLAYLIST_ID + "=?", args);
      }
    });
  }

  @Override
  public void addToPlaylist(final long playlistId, final long trackId) {
    DbUtils.write(this, new DbWriter() {
      @Override
      public void write(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(KEY_PLAYLIST_ID, playlistId);
        values.put(KEY_TRACK_ID, trackId);
        db.insert(TABLE_PLAYLIST_TRACKS, null, values);
        for (DbObserver observer : getObservers()) {
          observer.trackAdded(libraryManager.getTrack(trackId));
        }
      }
    });
  }

  @Override
  public void removeFromPlaylist(final long playlistId, final long trackId) {
    DbUtils.write(this, new DbWriter() {
      @Override
      public void write(SQLiteDatabase db) {
        String[] args = {"" + playlistId, "" + trackId};
        db.delete(TABLE_PLAYLIST_TRACKS, KEY_PLAYLIST_ID + "=? AND " + KEY_TRACK_ID + "=?", args);
      }
    });
  }

  @Override
  public List<Track> getTracksByPlayList(final long playlistId) {
    return DbUtils.read(this, new DbReader<List<Track>>() {
      @Override
      public List<Track> read(SQLiteDatabase db) {

        String[] columns = {KEY_TRACK_ID};
        String[] args = {"" + playlistId};

        Cursor cursor = db.query(TABLE_PLAYLIST_TRACKS, columns, KEY_PLAYLIST_ID + "=?", args, null, null, null);
        List<Track> tList = new ArrayList<Track>();
        if (cursor.moveToFirst()) {
          do {
            tList.add(libraryManager.getTrack(cursor.getLong(0))); // this needs to be optimized
          } while (cursor.moveToNext());
        }

        return tList;
      }
    });
  }
}