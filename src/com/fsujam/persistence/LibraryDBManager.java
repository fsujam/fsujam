package com.fsujam.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.fsujam.model.Album;
import com.fsujam.model.Artist;
import com.fsujam.model.Track;
import com.fsujam.persistence.utils.DbObserver;
import com.fsujam.persistence.utils.DbReader;
import com.fsujam.persistence.utils.DbUtils;
import com.fsujam.persistence.utils.DbWriter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Rowell Belen
 */
public class LibraryDBManager extends BaseDBManager implements LibraryManager {

  private static final int DATABASE_VERSION = 2;
  private static final String DATABASE_NAME = "FSUJam";
  private static final String TABLE_NAME = "Tracks";

  private static final String KEY_ID = "id";
  private static final String KEY_TITLE = "title";
  private static final String KEY_NUMBER = "number";
  private static final String KEY_ARTIST = "artist";
  private static final String KEY_ALBUM = "album";
  private static final String KEY_LENGTH = "length";
  private static final String KEY_SRC = "src";
  private static final String KEY_ROOTSRC = "rootSrc";
  private static final String KEY_ALBUMART = "hasAlbumArt";

  public LibraryDBManager(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
       + KEY_ID + " INTEGER PRIMARY KEY, "
       + KEY_TITLE + " TEXT, "
       + KEY_NUMBER + " INTEGER, "
       + KEY_ARTIST + " TEXT, "
       + KEY_ALBUM + " TEXT, "
       + KEY_LENGTH + " REAL, "
       + KEY_SRC + " TEXT, "
       + KEY_ROOTSRC + " TEXT, "
       + KEY_ALBUMART + " TEXT);";
    db.execSQL(CREATE_TABLE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db,
                        int oldVersion,
                        int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    onCreate(db);
  }

  @Override
  protected String[] getTableNamesToClear() {
    return new String[]{TABLE_NAME};
  }

  @Override
  protected String getTableNameToCheckForEmpty() {
    return TABLE_NAME;
  }

  @Override
  public void add(final Track track) {
    DbUtils.write(this, new DbWriter() {
      @Override
      public void write(SQLiteDatabase db) {
        ContentValues values = new ContentValues();

        values.put(KEY_TITLE, track.getTitle());
        values.put(KEY_NUMBER, track.getNumber());
        values.put(KEY_ARTIST, track.getArtist().getName());
        values.put(KEY_ALBUM, track.getAlbum().getName());
        values.put(KEY_LENGTH, track.getLength());
        values.put(KEY_SRC, track.getSrc());
        values.put(KEY_ROOTSRC, track.getRootSrc());
        values.put(KEY_ALBUMART, track.getAlbumArt());

        db.insert(TABLE_NAME, null, values);

        for (DbObserver observer : getObservers()) {
          observer.trackAdded(track);
        }
      }
    });
  }

  private Track toTrack(Cursor cursor) {
    Track track = new Track(
       cursor.getString(1),
       Integer.parseInt(cursor.getString(2)),
       new Artist(cursor.getString(3)),
       new Album(cursor.getString(4)),
       cursor.getDouble(5),
       cursor.getString(6),
       cursor.getString(7),
       cursor.getString(8)
    );
    track.setTrackId(Integer.parseInt(cursor.getString(0)));
    return track;
  }

  @Override
  public List<Track> getTracks() {
    return DbUtils.read(this, new DbReader<List<Track>>() {
      @Override
      public List<Track> read(SQLiteDatabase db) {
        List<Track> tList = new ArrayList<Track>();

        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
          do {
            Track t = toTrack(cursor);
            tList.add(t);
          } while (cursor.moveToNext());
        }

        return tList;
      }
    });
  }

  @Override
  public Track getTrack(final long trackId) {
    return DbUtils.read(this, new DbReader<Track>() {
      @Override
      public Track read(SQLiteDatabase db) {

      String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_ID + "=?";
      String[] args = {"" + trackId};
      Cursor cursor = db.rawQuery(query, args);
      List<Track> tList = new ArrayList<Track>();
      if (cursor.moveToFirst()) {
        do {
          tList.add(toTrack(cursor));
        } while (cursor.moveToNext());
      }

      // either not found or has multiple entries - error!
      if (tList.size() != 1) {
        return null;
      }

      return tList.get(0);
      }
    });
  }

  @Override
  public Set<Artist> getArtists() {
    Set<Artist> artists = new HashSet<Artist>();
    for(Track t : this.getTracks()){
      artists.add(t.getArtist());
    }
    return artists;
  }

  @Override
  public Set<Track> getTracksByArtist(String artistName) {
    Set<Track> tracks = new HashSet<Track>();
    for(Track t : this.getTracks()){
      if(t.getArtist().getName().equalsIgnoreCase(artistName)){
        tracks.add(t);
      }
    }
    return tracks;
  }
}