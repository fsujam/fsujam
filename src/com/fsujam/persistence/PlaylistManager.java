package com.fsujam.persistence;

import com.fsujam.model.Playlist;
import com.fsujam.model.Track;
import com.fsujam.persistence.utils.DbObserverAware;

import java.util.List;

/**
 * @author Rowell Belen
 */
public interface PlaylistManager extends DbObserverAware {

  public boolean isEmpty();

  public void clear();

  public List<Playlist> getPlaylists();

  public Playlist createNewPlaylist(String playlistName);

  public void deletePlaylist(long playlistId);

  public void renamePlaylist(long playlistId, String newPlaylistName);

  public void addToPlaylist(long playlistId, long trackId);

  public void removeFromPlaylist(long playlistId, long trackId);

  public List<Track> getTracksByPlayList(long playlistId);
}
