package com.fsujam.persistence;

import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.fsujam.persistence.utils.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rowell Belen
 */
public abstract class BaseDBManager extends SQLiteOpenHelper implements DbObserverAware {

  private List<DbObserver> observers = new ArrayList<DbObserver>();

  public BaseDBManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
    super(context, name, factory, version);
  }

  protected List<DbObserver> getObservers() {
    return observers;
  }

  public void addObserver(DbObserver observer) {
    observers.add(observer);
  }

  public void removeObserver(DbObserver observer) {
    observers.remove(observer);
  }

  public void clear() {
    DbUtils.write(this, new DbWriter() {
      @Override
      public void write(SQLiteDatabase db) {
        for(String tableName : getTableNamesToClear()){
          db.execSQL("DELETE FROM " + tableName);
        }
        for (DbObserver observer : getObservers()) {
          observer.cleaned();
        }
      }
    });
  }

  public boolean isEmpty() {
    return DbUtils.read(this, new DbReader<Boolean>() {
      @Override
      public Boolean read(SQLiteDatabase db) {
        return DatabaseUtils.queryNumEntries(db, getTableNameToCheckForEmpty()) == 0;
      }
    });
  }

  protected abstract String[] getTableNamesToClear();
  protected abstract String getTableNameToCheckForEmpty();
}
